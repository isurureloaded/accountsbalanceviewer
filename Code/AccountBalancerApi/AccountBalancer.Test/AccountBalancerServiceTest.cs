﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AccountBalancer.DAL.Entitities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountBalancer.Common.Enums;
using AccountBalancer.DAL;
using AccountBalancer.Infrastructure.Repository;
using AccountBalancer.Infrastructure.Service;
using Moq;
using AccountBalancer.Infrastructure.Interfaces;

namespace AccountBalancer.Test
{
    [TestClass]
    public class AccountBalancerServiceTest
    {

        [TestMethod]
        [Owner("Isuru")]
        public void Should_Generate_Report_Succesfully()
        {
            //Create mock account balance infos
            var accountBalanceInfos = new List<AccountBalanceInfo>
            {
               new AccountBalanceInfo()
               {
                   AccountType = AccountTypes.Canteen,
                   Balance = 100,
                   CreatedDateTime = DateTimeOffset.UtcNow,
                   UpdatedDateTime = DateTimeOffset.UtcNow,
                   Month = Months.April,
                   Year = 2005
               },
                new AccountBalanceInfo()
                {
                    AccountType = AccountTypes.Canteen,
                    Balance = 300,
                    CreatedDateTime = DateTimeOffset.UtcNow,
                    UpdatedDateTime = DateTimeOffset.UtcNow,
                    Month = Months.April,
                    Year = 2005
                },
                new AccountBalanceInfo()
                {
                    AccountType = AccountTypes.Canteen,
                    Balance = 400,
                    CreatedDateTime = DateTimeOffset.UtcNow,
                    UpdatedDateTime = DateTimeOffset.UtcNow,
                    Month = Months.August,
                    Year = 2005
                },
                new AccountBalanceInfo()
                {
                    AccountType = AccountTypes.Canteen,
                    Balance = 400,
                    CreatedDateTime = DateTimeOffset.UtcNow,
                    UpdatedDateTime = DateTimeOffset.UtcNow,
                    Month = Months.August,
                    Year = 2006
                }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<AccountBalanceInfo>>();
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.Provider).Returns(accountBalanceInfos.Provider);
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.Expression).Returns(accountBalanceInfos.Expression);
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.ElementType).Returns(accountBalanceInfos.ElementType);
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.GetEnumerator()).Returns(() => accountBalanceInfos.GetEnumerator());

            var mockContext = new Mock<IAccountContext>();
            mockContext.Setup(c => c.AccountBalanceInfoes).Returns(mockSet.Object);
            mockContext.Setup(c => c.Set<AccountBalanceInfo>()).Returns(mockSet.Object);

            var accountBalancerService = new AccountBalancerService(new UnitOfWork(mockContext.Object));

            var reportModel = accountBalancerService.GenerateReport(2005);

            Assert.IsNotNull(reportModel, "Report not succesfully generated");
            CollectionAssert.AreEqual(reportModel.Canteen, new List<decimal>() { 100, 300, 400 }, "Canteen report filteration is invalid");
            //Todo other assert for check the report model

        }

        [TestMethod]
        [Owner("Isuru")]
        public void Should_Return_Empty_Model_When_Not_Any_Account_Balance_Info()
        {
            var accountBalanceInfos = new List<AccountBalanceInfo>
            {
                new AccountBalanceInfo()
                {
                    AccountType = AccountTypes.Canteen,
                    Balance = 400,
                    CreatedDateTime = DateTimeOffset.UtcNow,
                    UpdatedDateTime = DateTimeOffset.UtcNow,
                    Month = Months.August,
                    Year = 2006
                }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<AccountBalanceInfo>>();
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.Provider).Returns(accountBalanceInfos.Provider);
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.Expression).Returns(accountBalanceInfos.Expression);
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.ElementType).Returns(accountBalanceInfos.ElementType);
            mockSet.As<IQueryable<AccountBalanceInfo>>().Setup(m => m.GetEnumerator()).Returns(() => accountBalanceInfos.GetEnumerator());

            var mockContext = new Mock<IAccountContext>();
            mockContext.Setup(c => c.AccountBalanceInfoes).Returns(mockSet.Object);
            mockContext.Setup(c => c.Set<AccountBalanceInfo>()).Returns(mockSet.Object);

            var accountBalancerService = new AccountBalancerService(new UnitOfWork(mockContext.Object));

            var reportModel = accountBalancerService.GenerateReport(2005);

            Assert.IsTrue(reportModel.Canteen.Count == 0 && reportModel.CeoCar.Count == 0 &&
                          reportModel.Marketing.Count == 0 && reportModel.Months.Count == 0 &&
                          reportModel.ParkingFines.Count == 0 && reportModel.RnD.Count == 0);
        }
    }
}
