﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountBalancer.WebApi.Helpers
{
    public static class FileExtensionValidator
    {
        /// <summary>
        /// Check uploaded file is in excel format
        /// </summary>
     
        public static bool CheckExcelFormat(string fileExtension)
        {          
            if (fileExtension.ToLower() == ".xlsx") return true;
            else if (fileExtension.ToLower() == ".xls") return true;
            else return false;
        }
    }
}