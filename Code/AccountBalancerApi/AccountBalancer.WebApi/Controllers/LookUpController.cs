﻿using AccountBalancer.Common.Enums;
using AccountBalancer.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AccountBalancer.WebApi.Controllers
{
    [Authorize]
    public class LookUpController : ApiController
    {

        public IHttpActionResult GetMonths()
        {
            var months = EnumExtension.ToEnumMembers<Months>();
            return Json(months);
        }
    }
}
