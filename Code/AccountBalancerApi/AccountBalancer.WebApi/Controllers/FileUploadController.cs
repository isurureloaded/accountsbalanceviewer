﻿using AccountBalancer.Common.Enums;
using AccountBalancer.Common.Extensions;
using AccountBalancer.DAL.Entitities;
using AccountBalancer.Infrastructure.Interfaces;
using AccountBalancer.WebApi.Helpers;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AccountBalancer.WebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FileUploadController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountBalancerService _accountService;

        public FileUploadController(IUnitOfWork unitOfWork, IAccountBalancerService accountService)
        {
            _unitOfWork = unitOfWork;
            _accountService = accountService;
        }

        [HttpPost]
        public HttpResponseMessage FileUpload()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                AccountBalanceInfo accountData = new AccountBalanceInfo();

                var accountDataString = httpRequest.Form["accountData"];

                //check whether a file is uploaded
                if (httpRequest.Files == null || httpRequest.Files.Count == 0 || string.IsNullOrEmpty(accountDataString))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "There are no files");

                accountData = new JavaScriptSerializer().Deserialize<AccountBalanceInfo>(accountDataString);

                //check account balance is already uploaded or not
                if (_accountService.IsAccountBalanceExists(accountData.Year, accountData.Month))
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Already data has been uploaded");
                }

                foreach (string fileIndex in httpRequest.Files)
                {
                    var file = httpRequest.Files[fileIndex];
                    string strpath = Path.GetExtension(file.FileName);

                    if (!FileExtensionValidator.CheckExcelFormat(strpath)) return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid file type");

                    //read data from uploaded excel sheet
                    using (var reader = ExcelReaderFactory.CreateReader(file.InputStream))
                    {
                        while (reader.Read())
                        {
                            var accountBalance = new AccountBalanceInfo
                            {
                                AccountType = EnumExtension.GetValueFromName<AccountTypes>(reader.GetString(0)),
                                Month = (Months)accountData.Month,
                                Balance = Convert.ToDecimal(reader.GetDouble(1)),
                                Year = accountData.Year,
                            };

                            //save account balance details to db
                            _unitOfWork.AccountBalanceInfoRepository.Insert(accountBalance);
                            _unitOfWork.Save();
                        }

                    }

                }
                return Request.CreateErrorResponse(HttpStatusCode.OK, "File uploaded successfuly");
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error");
            }

        }
    }


}
