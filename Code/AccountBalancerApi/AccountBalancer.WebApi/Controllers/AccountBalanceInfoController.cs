﻿using AccountBalancer.Common.Enums;
using AccountBalancer.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AccountBalancer.WebApi.Controllers
{
    [RoutePrefix("api/AccountBalanceInfo")]
    [Authorize]
    public class AccountBalanceInfoController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountBalancerService _accountBalancerService;


        public AccountBalanceInfoController(IUnitOfWork unitOfWork, IAccountBalancerService accountBalancerService)
        {
            _unitOfWork = unitOfWork;
            _accountBalancerService = accountBalancerService;
        }

        [Route("GetAccountBalanceInfo")]
        public IHttpActionResult GetAccountBalanceInfo(int year, int month)
        {
            var accountBalanceInfo = _unitOfWork.AccountBalanceInfoRepository
                .Get(a => a.Year == year && a.Month == (Months)month).ToList();

            Dictionary<string, decimal> accountBalances = new Dictionary<string, decimal>();

            foreach (var item in accountBalanceInfo)
            {
                accountBalances.Add(item.AccountType.ToString(), item.Balance);
            }

            return Ok(accountBalances);
        }

        [Route("GenerateReport")]
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IHttpActionResult GenerateReport(int year)
        {
            var reportModel = _accountBalancerService.GenerateReport(year);
            return Ok(reportModel);
        }

       
    }
}
