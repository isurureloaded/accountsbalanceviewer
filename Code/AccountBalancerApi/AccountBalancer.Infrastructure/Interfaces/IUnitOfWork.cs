﻿using AccountBalancer.DAL.Entitities;
using AccountBalancer.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.Infrastructure.Interfaces
{
    public interface IUnitOfWork
    {
        void Save();
        void Dispose();
        IGenericRepository<AccountBalanceInfo> AccountBalanceInfoRepository { get; }

    }
}
