﻿using AccountBalancer.Common.Enums;
using AccountBalancer.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.Infrastructure.Interfaces
{
    public interface IAccountBalancerService
    {
        ReportModel GenerateReport(int year);
        bool IsAccountBalanceExists(int year, Months month);
    }
}
