﻿using AccountBalancer.Common.Enums;
using AccountBalancer.Common.Models;
using AccountBalancer.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.Infrastructure.Service
{
    public class AccountBalancerService : IAccountBalancerService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AccountBalancerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get data to generate the report according to selected year
        /// </summary>

        public ReportModel GenerateReport(int year)
        {
            var accountBalaneInfoes = _unitOfWork.AccountBalanceInfoRepository.Get(a => a.Year == year);

            var accountBalaneInfoGrouped = (from acc in accountBalaneInfoes
                                            orderby acc.Month
                                            group acc by acc.AccountType into g
                                            select new
                                            {
                                                Types = g.Key,
                                                Prices = g.Select(c => c.Balance).ToList(),
                                                Months = g.Select(c => c.Month).ToList()
                                            }
                   ).ToList();


            var Canteen = accountBalaneInfoGrouped.FirstOrDefault(v => v.Types == AccountTypes.Canteen);
            var RnD = accountBalaneInfoGrouped.FirstOrDefault(v => v.Types == AccountTypes.RAndD);
            var Marketing = accountBalaneInfoGrouped.FirstOrDefault(v => v.Types == AccountTypes.Marketing);
            var CeoCar = accountBalaneInfoGrouped.FirstOrDefault(v => v.Types == AccountTypes.CeoCarExpenses);
            var ParkingFines = accountBalaneInfoGrouped.FirstOrDefault(v => v.Types == AccountTypes.ParkingFines);
            var Months = accountBalaneInfoGrouped.FirstOrDefault();

            var report = new ReportModel()
            {
                Canteen = Canteen != null ? Canteen.Prices : new List<decimal>(),
                RnD = RnD != null ? RnD.Prices : new List<decimal>(),
                Marketing = Marketing != null ? Marketing.Prices : new List<decimal>(),
                CeoCar = CeoCar != null ? CeoCar.Prices : new List<decimal>(),
                ParkingFines = ParkingFines != null ? ParkingFines.Prices : new List<decimal>(),
                Months = Months != null ? Months.Months.Select(a => a.ToString()).ToList() : new List<string>(),

            };

            return report;
        }

        /// <summary>
        /// Check Account balance exists for selected year and month
        /// </summary>

        public bool IsAccountBalanceExists(int year, Months month)
        {
            var accountBalance = _unitOfWork.AccountBalanceInfoRepository.Get(a => a.Year == year && a.Month == month).FirstOrDefault();
            if (accountBalance != null) return true;

            return false;
        }
    }
}
