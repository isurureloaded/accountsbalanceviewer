﻿using AccountBalancer.DAL;
using AccountBalancer.DAL.Entitities;
using AccountBalancer.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.Infrastructure.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IAccountContext _context;
        private bool _disposed = false;

        private IGenericRepository<AccountBalanceInfo> _accountBalanceRepository;

        public UnitOfWork(IAccountContext context)
        {
            _context = context;
        }

        public IGenericRepository<AccountBalanceInfo> AccountBalanceInfoRepository => 
            _accountBalanceRepository ?? (_accountBalanceRepository = new GenericRepository<AccountBalanceInfo>(_context));

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
