﻿using AccountBalancer.DAL.Entitities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.DAL
{
    public class AccountContext : IdentityDbContext<AccountUser>, IAccountContext
    {
        public AccountContext() : base("AccountConnection")
        {

        }

        public DbSet<AccountBalanceInfo> AccountBalanceInfoes { get; set; }
    }
}
