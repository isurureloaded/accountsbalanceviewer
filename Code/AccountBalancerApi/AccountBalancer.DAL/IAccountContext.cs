﻿using AccountBalancer.DAL.Entitities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.DAL
{
    public interface IAccountContext
    {
        DbSet<AccountBalanceInfo> AccountBalanceInfoes { get; set; }
        int SaveChanges();
        void Dispose();
        DbSet<T> Set<T>() where T : class;
        DbEntityEntry<T> Entry<T>(T entity) where T : class;
    }
}
