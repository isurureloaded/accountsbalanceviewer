namespace AccountBalancer.DAL.Migrations
{
    using Entitities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AccountBalancer.DAL.AccountContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AccountBalancer.DAL.AccountContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            CreateRoles(context);
            CreateUsers(context);
        }

        private void CreateUsers(AccountContext context)
        {
            var userManager = new UserManager<AccountUser>(new UserStore<AccountUser>(context));

            if (userManager.FindByName("Admin") == null)
            {
                var user = new AccountUser { UserName = "Admin" };
                var result = userManager.Create(user, "123456");

                if (!result.Succeeded)
                {
                    throw new Exception(string.Format("Admin User couldn't be created. {0}", string.Join(",", result.Errors)));
                }

                userManager.AddToRole(user.Id, "Admin");

            }

            if (userManager.FindByName("User") == null)
            {
                var user = new AccountUser() { UserName = "User" };
                var result = userManager.Create(user, "123qwe");

                if (!result.Succeeded)
                {
                    throw new Exception(string.Format("User couldn't be created. {0}", string.Join(",", result.Errors)));
                }

                userManager.AddToRole(user.Id, "User");

            }

        }

        private static void CreateRoles(AccountContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            foreach (string roleName in new string[] { "Admin", "User" })
            {
                if (!roleManager.RoleExists(roleName))
                {
                    roleManager.Create(new IdentityRole(roleName));
                }
            }
        }
    }
}
