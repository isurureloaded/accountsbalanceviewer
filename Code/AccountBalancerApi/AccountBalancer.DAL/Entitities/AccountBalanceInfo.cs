﻿using AccountBalancer.Common.Enums;
using AccountBalancer.DAL.Entitities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.DAL.Entitities
{
    public class AccountBalanceInfo: BaseEntity
    {
        public int Year { get; set; }
        public decimal Balance { get; set; }
        public AccountTypes AccountType { get; set; }
        public Months Month { get; set; }
      
    }
}
