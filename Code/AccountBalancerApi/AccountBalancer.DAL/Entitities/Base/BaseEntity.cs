﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.DAL.Entitities.Base
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            CreatedDateTime = UpdatedDateTime = DateTime.UtcNow;
        }
        [Key]
        public int Id { get; set; }
        public DateTimeOffset CreatedDateTime { get; set; }
        public DateTimeOffset UpdatedDateTime { get; set; }
    }
}
