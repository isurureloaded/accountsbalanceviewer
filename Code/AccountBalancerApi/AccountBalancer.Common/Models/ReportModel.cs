﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.Common.Models
{
    public class ReportModel
    {
        public List<string> Months { get; set; }
        public List<decimal> RnD { get; set; }
        public List<decimal> Canteen { get; set; }
        public List<decimal> CeoCar { get; set; }
        public List<decimal> Marketing { get; set; }
        public List<decimal> ParkingFines { get; set; }
    }
}
