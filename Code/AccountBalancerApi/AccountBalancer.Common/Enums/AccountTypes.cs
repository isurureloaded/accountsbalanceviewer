﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.Common.Enums
{
    public enum AccountTypes
    {     
        [Display(Name = "R&D")]
        RAndD,
        Canteen,
        [Display(Name = "CEO’s car")]
        CeoCarExpenses,
        Marketing,
        [Display(Name = "Parking fines")]
        ParkingFines
    }
}
