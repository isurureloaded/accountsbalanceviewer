﻿using AccountBalancer.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AccountBalancer.Common.Extensions
{
    public static class EnumExtension
    {
        public static List<EnumMember> ToEnumMembers<T>()
        {
            Type type = typeof(T);

            if (!type.GetTypeInfo().IsEnum)
            {
                throw new ArgumentException("T must be of type enumeration.");
            }

            return Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(e => new EnumMember()
                {
                    Name = e.ToString(),
                    Value = ((IConvertible)e).ToInt32(null),
                    DisplayName = e.GetDisplayName()
                })
                .ToList();
        }

        public static string GetDisplayName(this object enumObject)
        {
            var field = enumObject.GetType()
                .GetField(enumObject.ToString());

            if (field != null)
            {
                var display = ((DisplayAttribute[])field.GetCustomAttributes(typeof(DisplayAttribute), false))
                    .FirstOrDefault();

                if (display != null)
                {
                    return display.Name;
                }
            }

            return enumObject.ToString();
        }

        public static T GetValueFromName<T>(string name)
        {
            var z = "CEO’s car";
            var y = "notequal";
            if (name == z) { y = "equal"; };

            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DisplayAttribute)) as DisplayAttribute;
                if (attribute != null)
                {
                    if (attribute.Name == name.Trim())
                    {
                        return (T)field.GetValue(null);
                    }
                }
                else
                {
                    if (field.Name == name)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentOutOfRangeException("name");
        }
    }
}
