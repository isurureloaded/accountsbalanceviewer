import { AccountBalancerUIPage } from './app.po';

describe('account-balancer-ui App', () => {
  let page: AccountBalancerUIPage;

  beforeEach(() => {
    page = new AccountBalancerUIPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
