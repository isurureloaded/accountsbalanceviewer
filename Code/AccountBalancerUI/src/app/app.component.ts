import { BlockUIService } from './modules/core/services/blockUI.service';
import { Component, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';
  isBlocked: boolean;

  constructor(private blockUIService: BlockUIService, vcr: ViewContainerRef, private toastr: ToastsManager) {
    this.toastr.setRootViewContainerRef(vcr);
    blockUIService.start$.subscribe(message => { this.isBlocked = message; });
  }

  isLoggedIn() {
    const token = localStorage.getItem('id_token');
    if (token != null) return true;
    return false;
  }
}
