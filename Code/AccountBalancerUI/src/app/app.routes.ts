import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders, Component } from '@angular/core';
import { LoginComponent } from './modules/login/login.component';



export const routes: Routes = [

  {
    path: '', component: LoginComponent,
  },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules });