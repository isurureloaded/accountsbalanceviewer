import { AuthenticationGuard } from './modules/login/auth.guard';
import { LoginModule } from './modules/login/login.module';
import { routing } from './app.routes';
import { AccountBalancerModule } from './modules/account-balancer/account-balancer.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CoreModule } from './modules/core/core.module';
import { SharedModule } from './modules/shared/shared.module';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';




@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
   SharedModule,
    CoreModule.forRoot(),
    routing,
    AccountBalancerModule,
    LoginModule
  ],
  providers: [AuthenticationGuard,{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
