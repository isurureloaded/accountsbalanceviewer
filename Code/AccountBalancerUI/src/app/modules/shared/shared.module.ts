import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { CustomOption } from './custom-option';
import { ToastOptions } from 'ng2-toastr';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        RouterModule,
        ToastModule.forRoot(),
     
    ],
    providers: [{ provide: ToastOptions, useClass: CustomOption }],
    exports: [FormsModule, HttpModule, CommonModule, RouterModule]
})

export class SharedModule {
}