import { ToastOptions } from 'ng2-toastr';

export class CustomOption extends ToastOptions {
    positionClass= 'toast-bottom-right';
    animate = 'flyRight'; // you can override any options available
    newestOnTop = false;
    showCloseButton = true;

}