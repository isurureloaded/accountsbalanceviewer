import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { BlockUIService } from './../../core/services/blockUI.service';
import { AccountBalancerService } from './../account-balancer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  years = [];
  months = [];
  accountData = { 'year': new Date().getFullYear(), 'month': new Date().getMonth() + 1 };

  fileList: FileList;
  constructor(private accountBalancerService: AccountBalancerService, private router: Router,
    private blockUIService: BlockUIService, private toaster: ToastsManager) { }

  ngOnInit() {

    this.getYears();
    this.getMonths();
  }


  getYears() {
    this.accountBalancerService.getYears().subscribe(data => { this.years = data; console.log(data); });
  }

  getMonths() {
    this.accountBalancerService.getMonths().subscribe(data => { this.months = data; });
  }

  selectFile(event) {
    this.fileList = event.target.files;
  }

  fileUpload() {
    this.blockUIService.BlockUI(true);
    this.accountBalancerService.uploadFile(this.fileList, this.accountData).subscribe(data => {
      this.router.navigate(["/accounts"]);
    },
      error => {
        this.blockUIService.BlockUI(false);
        this.toaster.error(error, 'ERROR!');
      },
      () => { this.blockUIService.BlockUI(false); }

    );
  }


}
