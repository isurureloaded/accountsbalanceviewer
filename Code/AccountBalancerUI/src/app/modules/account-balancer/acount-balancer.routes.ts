import { AuthenticationGuard } from './../login/auth.guard';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AccountBalancerComponent } from './account-balancer.component';
import { AccountBalanceDetailsComponent } from './account-balance-details/account-balance-details.component';
import { ReportsComponent } from './reports/reports.component';
import { FileUploadComponent } from './file-upload/file-upload.component';


export const routes: Routes = [

    {
        path: 'accounts',
        component: AccountBalancerComponent,
        canActivate:[AuthenticationGuard],
        children: [
            { path: '', component: AccountBalanceDetailsComponent },
            { path: 'reports', component: ReportsComponent },
            { path: 'fileupload', component: FileUploadComponent },

        ]
    },


];

export const RoutingAccounts: ModuleWithProviders = RouterModule.forChild(routes);
export const RoutableAccountsComponents = [
   AccountBalanceDetailsComponent,
   ReportsComponent,
   FileUploadComponent
];