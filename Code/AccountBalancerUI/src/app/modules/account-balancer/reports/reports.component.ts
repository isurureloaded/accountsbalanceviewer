import { BlockUIService } from './../../core/services/blockUI.service';
import { AccountBalancerService } from './../account-balancer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  options: Object;
  years = [];
  year = new Date().getFullYear();

  constructor(private accountBalancerService: AccountBalancerService,
   private blockUIService: BlockUIService) {

  }

  ngOnInit() {
    this.getYears();
    this.getReportData();
  }

  getReportData() {
    this.blockUIService.BlockUI(true);
    this.accountBalancerService.getReportData(this.year).subscribe(data => {
      this.generateReport(data);
    }, error => { this.blockUIService.BlockUI(false); },
      () => { this.blockUIService.BlockUI(false); });

  }

  getYears() {
    this.accountBalancerService.getYears().subscribe(data => { this.years = data; console.log(data); });
  }


  generateReport(reportData) {

    if (reportData == null) return;

    this.options = {
      title: { text: 'Account Balance Details '+this.year },
      xAxis: {
        categories: reportData.Months,
      },
      
      series: [{
        name: 'R&D',
        data: reportData.RnD
      }, {
        name: 'Canteen',
        data: reportData.Canteen
      }, {
        name: 'CEOs Car',
        data: reportData.CeoCar
      }, {
        name: 'Marketing',
        data: reportData.Marketing
      }, {
        name: 'Parking Fine',
        data: reportData.ParkingFines
      }]
    };
  }
}
