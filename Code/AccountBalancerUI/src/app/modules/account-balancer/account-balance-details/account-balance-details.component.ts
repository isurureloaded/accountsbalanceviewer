import { BlockUIService } from './../../core/services/blockUI.service';
import { AccountBalancerService } from './../account-balancer.service';
import { Component, OnInit } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-account-balance-details',
  templateUrl: './account-balance-details.component.html',
  styleUrls: ['./account-balance-details.component.scss']
})
export class AccountBalanceDetailsComponent implements OnInit {

  years = [];
  months = [];
  acountBalanceInfo = [];
  year = new Date().getFullYear();
  month = new Date().getMonth() + 1;

  constructor(private accountBalancerService: AccountBalancerService,
    private blockUIService: BlockUIService, private toaster: ToastsManager) { }

  ngOnInit() {
    this.getYears();
    this.getMonths();
    this.getAcountData();

  }

  getYears() {

    this.accountBalancerService.getYears().subscribe(data => { this.years = data; console.log(data); });
  }

  getMonths() {
    this.accountBalancerService.getMonths().subscribe(data => { this.months = data; });
  }

  getAcountData() {
    this.blockUIService.BlockUI(true);
    this.accountBalancerService.getAcountData(this.year, this.month).subscribe(data => {
      this.acountBalanceInfo = data;
     
    }, error => {
      this.blockUIService.BlockUI(false);
      this.toaster.error("Sorry !! Error has occured", 'ERROR!');
    },
      () => { this.blockUIService.BlockUI(false); });
  }
}
