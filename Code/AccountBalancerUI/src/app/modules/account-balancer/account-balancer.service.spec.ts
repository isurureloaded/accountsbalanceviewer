import { TestBed, inject } from '@angular/core/testing';

import { AccountBalancerService } from './account-balancer.service';

describe('AccountBalancerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountBalancerService]
    });
  });

  it('should be created', inject([AccountBalancerService], (service: AccountBalancerService) => {
    expect(service).toBeTruthy();
  }));
});
