import { HttpClient } from './../core/services/HttpClient';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AccountBalancerService {

  private apiEndPoint = environment.apiEndPoint;
  constructor(private http: HttpClient) { }

  getTestData() {
    return this.http.get(this.apiEndPoint + 'Values').map(res => res).catch(this.handleError);
  }

  getYears(): Observable<any> {
    const currentYear = new Date().getFullYear();
    const years = [];
    for (let i = 0; i < 5; i++) {
      years.push(currentYear - i);
    }

    return Observable.create(observable => { observable.next(years); });
  }

  getMonths() {
    return this.http.get(this.apiEndPoint + 'LookUp/GetMonths').map(this.extractData).catch(this.handleError);
  }

  uploadFile(files, attachmentData) {
    const formData: FormData = new FormData();

    if (files != null && files != undefined && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append('uploadFile' + i, files[i]);
      }
    }
    formData.append('accountData', JSON.stringify(attachmentData));
    return this.http.postFiles(this.apiEndPoint + 'FileUpload/FileUpload', formData).map(this.extractData).catch(this.handleError);
  }

  getReportData(year: number) {
    return this.http.get(this.apiEndPoint + 'AccountBalanceInfo/GenerateReport?year=' + year).map(this.extractData).catch(this.handleError);
  }

  getAcountData(year: number, month: number) {
    return this.http.get(this.apiEndPoint + 'AccountBalanceInfo/GetAccountBalanceInfo?year=' + year + '&month=' + month).map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response) {
    console.log(res);
    if (res['_body']) {
      const data = JSON.parse(res['_body']);
      if (data.value)
      { return data.value; }
      else
      { return data; }
    }
  }

  private handleError(error: Response | any) {

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      errMsg = body.Message || errMsg;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
   
    return Observable.throw(errMsg);
  }

}
