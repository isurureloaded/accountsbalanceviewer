import { SharedModule } from './../shared/shared.module';
import { AccountBalancerComponent } from './account-balancer.component';
import { AccountBalancerService } from './account-balancer.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartModule } from 'angular2-highcharts';
import { RoutingAccounts, RoutableAccountsComponents } from './acount-balancer.routes';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';

declare var require: any;
export function highchartsFactory() {
  return require('highcharts');
}


@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    RoutingAccounts,
    SharedModule
    
  ],
  declarations: [RoutableAccountsComponents, AccountBalancerComponent],
  providers: [AccountBalancerService, {
    provide: HighchartsStatic,
    useFactory: highchartsFactory
  }]
})
export class AccountBalancerModule { }
