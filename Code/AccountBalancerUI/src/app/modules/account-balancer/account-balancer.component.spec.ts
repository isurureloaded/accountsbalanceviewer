import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountBalancerComponent } from './account-balancer.component';

describe('AccountBalancerComponent', () => {
  let component: AccountBalancerComponent;
  let fixture: ComponentFixture<AccountBalancerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountBalancerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountBalancerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
