
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(private router: Router, private _toastr: ToastsManager) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        
        if ((localStorage.getItem("id_token") == null || localStorage.getItem("id_token") == undefined)) {
            return false;
        }
        else {
            return true;
        }
    }


}


