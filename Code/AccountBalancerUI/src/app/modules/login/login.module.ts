import { SharedModule } from './../shared/shared.module';
import { AuthService } from './auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
  ],
  declarations: [LoginComponent],
  providers: [AuthService]
})
export class LoginModule { }
