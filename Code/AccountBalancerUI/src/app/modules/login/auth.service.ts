import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {

    tokenUrl = environment.tokenEndPoint;

    constructor(private http: Http, private router: Router) { }

    login(user) {


        const c = 'userName=' + user.userName +
            '&password=' + user.password +
            '&grant_type=password';

        const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(this.tokenUrl, c, options).map(res => res.json()).catch(this.handleError);

    }
    private handleError(error: Response | any) {

        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            errMsg = body.error_description || JSON.stringify(body);          
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw(errMsg);
    }
}