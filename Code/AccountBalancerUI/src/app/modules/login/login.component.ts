import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from './auth.service';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authervice: AuthService, private router: Router, private http: Http,
    private toaster: ToastsManager) { }

  user = { 'userName': '', 'password': '' };
  loggedIn = false;
  showSpinner = false;

  isloginFormSubmitted = false;
  @ViewChild('loginForm') loginFormNormal;
  tokenUrl = environment.tokenEndPoint;

  ngOnInit() {
    if (localStorage.getItem('id_token')) this.router.navigate(['/accounts']);
  }

  signIn() {


    this.isloginFormSubmitted = true;
    if (this.loginFormNormal.invalid) return;
    this.showSpinner = true;
    this.authervice.login(this.user).subscribe(
      response => {
      
        localStorage.setItem('id_token', response.access_token);
        localStorage.setItem('username', response.userName);
        localStorage.setItem('userrole', response.rolename);

        this.router.navigate(['/accounts']);

      },
      error => {
      
        this.toaster.error(error, 'ERROR!');
        this.showSpinner = false;
        this.isloginFormSubmitted = false;

      }, () => {
        this.showSpinner = false;
        this.isloginFormSubmitted = false;
      }
    );
  }

}
